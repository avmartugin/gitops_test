# gitops_test

## Wireguard

```
sudo apt update
sudo apt upgrade -y
sudo apt install wireguard-tools neovim -y
sudo wg genkey | tee privatekey | wg pubkey > publickey
sudo nvim /etc/sysctl.conf
```

uncomment net.ipv4.ip_forward=1
sysctl -p
net.ipv6.conf.all.forwarding = 1
net.ipv4.ip_forward = 1

```
sudo nvim /etc/wireguard/wg0.conf
```

[Interface]
Address = 10.0.0.2/32
PrivateKey = 6CnexiI80kx/KFCH1V6um2Ap8pzianf/ESkId+fxkEE=
ListenPort = 51820
DNS = 1.1.1.1

[Peer]
PublicKey = JtDPs/ZKu0rEAdi/8q8cSwHrGnPx93dbHFzWhdus23Y=
Endpoint = 178.170.192.170:51820
AllowedIPs = 10.0.0.0/24
PersistentKeepalive = 25

```
sudo systemctl enable wg-quick@wg0
sudo wg-quick up wg0
sudo wg show
```

## Mount

```
sudo mkdir -p /opt/yamls
sudo mkdir -p /opt/secrets
sudo mkdir -p /opt/registry
sudo mkdir -p /opt/pipelines
sudo mkdir -p /opt/minio
sudo mount 192.168.0.189:/opt/yamls /opt/yamls
sudo mount 192.168.0.189:/opt/secrets /opt/secrets
sudo mount 192.168.0.189:/opt/registry /opt/registry
sudo mount 192.168.0.189:/opt/pipelines /opt/pipelines
sudo mount 192.168.0.189:/opt/minio /opt/minio
```

## K3s

```
sudo /usr/local/bin/k3s-uninstall.sh
curl -sfL https://get.k3s.io | sh -s - --disable traefik --write-kubeconfig-mode 644
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
```

## PV PVC

```
kubectl create namespace registry
kubectl apply -f /opt/yamls/pv-pvc
```

## Helm

```
wget https://get.helm.sh/helm-v3.9.0-linux-amd64.tar.gz
tar -zxvf helm-v3.9.0-linux-amd64.tar.gz
chmod +x linux-amd64/helm
sudo mv linux-amd64/helm /usr/local/bin/helm
```

## Minio

```
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install minio bitnami/minio \
--set rootUser=user,rootPassword=passwd
```

## Loki & Grafana

```
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
helm upgrade --install loki grafana/loki-distributed
helm install loki-grafana grafana/grafana
kubectl get secret --namespace <YOUR-NAMESPACE> loki-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
kubectl port-forward --namespace <YOUR-NAMESPACE> service/loki-grafana 3000:3000
```

## Promtail

```
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
helm upgrade --install promtail grafana/promtail --set "loki.serviceName=loki"
```

## Secrets

### git-sa

```
kubectl create secret generic ssh-creds \
--from-file=id_rsa=/opt/secrets/gitlab-ssh/id_rsa \
--from-file=known_hosts=/opt/secrets/gitlab-ssh/known_hosts
cat <<EOF > git-sa.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: git-sa
secrets:
- name: ssh-creds
EOF
kubectl apply -f git-sa.yaml
```

### build-sa

```
kubectl create secret docker-registry registry-credentials \
  --docker-server=registry.gitlab.com \
  --docker-username=avmartugin \
  --docker-password=glpat--Hxirs7yyWtXY5cmoAbS
cat <<EOF > build-sa.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: build-sa
imagePullSecrets:
- name: registry-credentials
secrets:
- name: registry-credentials
EOF
kubectl apply -f build-sa.yaml
```

### knative-sa

```
cat <<EOF > knative-sa-cr-crb.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: knative-sa
  namespace: default
secrets:
- name: registry-credentials
imagePullSecrets:
- name: registry-credentials
---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: knative-cr
rules:
  - apiGroups: ["serving.knative.dev"]
    resources: ["services", "revisions", "routes"]
    verbs: ["get", "list", "create", "update", "delete", "patch", "watch"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: knative-crb
subjects:
- kind: ServiceAccount
  name: knative-sa
  namespace: default
roleRef:
  kind: ClusterRole
  name: knative-cr
  apiGroup: rbac.authorization.k8s.io
EOF
kubectl apply -f knative-sa-cr-crb.yaml
```

## Knative

```
kubectl apply -f https://github.com/knative/serving/releases/download/knative-v1.5.0/serving-crds.yaml
kubectl apply -f https://github.com/knative/serving/releases/download/knative-v1.5.0/serving-core.yaml
kubectl apply -f https://github.com/knative/serving/releases/download/knative-v1.5.0/serving-default-domain.yaml
kubectl apply -f https://github.com/knative/net-kourier/releases/download/knative-v1.5.0/kourier.yaml
kubectl patch configmap/config-network --namespace knative-serving --type merge --patch '{"data":{"ingress.class":"kourier.ingress.networking.knative.dev"}}'
```

## Tekton

```
kubectl apply -f https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml
kubectl apply -f https://storage.googleapis.com/tekton-releases/dashboard/latest/tekton-dashboard-release.yaml
```
wait
```
kubectl apply -f https://raw.githubusercontent.com/tektoncd/catalog/main/task/git-clone/0.6/git-clone.yaml
kubectl apply -f https://raw.githubusercontent.com/tektoncd/catalog/main/task/buildpacks/0.4/buildpacks.yaml
kubectl apply -f https://raw.githubusercontent.com/tektoncd/catalog/main/task/kubernetes-actions/0.2/kubernetes-actions.yaml
kubectl apply -f https://raw.githubusercontent.com/tektoncd/catalog/main/task/kn-apply/0.1/kn-apply.yaml
kubectl port-forward -n tekton-pipelines service/tekton-dashboard 9097:9097
kubectl edit -n tekton-pipelines service/tekton-dashboard 
```

```
kubectl apply -f /opt/yamls/pipelines/pipeline.yaml
kubectl apply -f /opt/yamls/pipelines/pipeline-run.yaml
```