fastapi==0.78.0
gunicorn==20.1.0 
uvicorn==0.17.6
minio==7.1.9 
loguru==0.6.0