import io

from fastapi import FastAPI
from loguru import logger
from minio import Minio
from minio.error import S3Error

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/minio")
async def minio():
    try:
        client = Minio(
            "minio.default.svc.cluster.local",
            access_key="QYRGNdj4iP33JMe1",
            secret_key="cuoxGCJIxs3Xv97rHPdhOGbu3DRrQyBa",
        )
        bucket_name = "testbucket"
        found = client.bucket_exists(bucket_name)
        if not found:
            client.make_bucket(bucket_name)
            logger.warning(f'{bucket_name} not exists, created')
        else:
            logger.info(f'{bucket_name} exists, fine')
        result = client.put_object(
            "testbucket", "hello-object", io.BytesIO(b"hello"))
        logger.info(
            "created {0} object; etag: {1}, version-id: {2}".format(
                result.object_name, result.etag, result.version_id,
            ),
        )
        return {"message": "minio done"}
    except S3Error as exc:
        logger.error(f'exception: {exc}')
        return {"error": str(exc)}
